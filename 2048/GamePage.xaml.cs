﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO;

namespace _2048
{
    public partial class Page1 : PhoneApplicationPage
    {
        public Page1()
        {
            InitializeComponent();
        }
        
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            TextBlockMove.Text += "0";
            StreamReader reader = new StreamReader("./PlayerName.txt");
            string value = reader.ReadLine();
            reader.Close();
            TextBlockPlayerName.Text = value;
            int firstNumber, secondNumber;
            Random rand = new Random();
            firstNumber = rand.Next(1,17);
            secondNumber = rand.Next (1,17);
            while (secondNumber == firstNumber)
            {
                secondNumber = rand.Next(1, 17);
            }
            TextB1.Text = (TextB1.Name.Substring(5) == firstNumber.ToString() || TextB1.Name.Substring(5) == secondNumber .ToString())?"2":"";
            TextB2.Text = (TextB2.Name.Substring(5) == firstNumber.ToString() || TextB2.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB3.Text = (TextB3.Name.Substring(5) == firstNumber.ToString() || TextB3.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB4.Text = (TextB4.Name.Substring(5) == firstNumber.ToString() || TextB4.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB5.Text = (TextB5.Name.Substring(5) == firstNumber.ToString() || TextB5.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB6.Text = (TextB6.Name.Substring(5) == firstNumber.ToString() || TextB6.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB7.Text = (TextB7.Name.Substring(5) == firstNumber.ToString() || TextB7.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB8.Text = (TextB8.Name.Substring(5) == firstNumber.ToString() || TextB8.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB9.Text = (TextB9.Name.Substring(5) == firstNumber.ToString() || TextB9.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB10.Text = (TextB10.Name.Substring(5) == firstNumber.ToString() || TextB10.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB11.Text = (TextB11.Name.Substring(5) == firstNumber.ToString() || TextB11.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB12.Text = (TextB12.Name.Substring(5) == firstNumber.ToString() || TextB12.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB13.Text = (TextB13.Name.Substring(5) == firstNumber.ToString() || TextB13.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB14.Text = (TextB14.Name.Substring(5) == firstNumber.ToString() || TextB14.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB15.Text = (TextB15.Name.Substring(5) == firstNumber.ToString() || TextB15.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";
            TextB16.Text = (TextB16.Name.Substring(5) == firstNumber.ToString() || TextB16.Name.Substring(5) == secondNumber.ToString()) ? "2" : "";

        }
        Point point;
        bool condition,notFound,notSaved=true;
        int recentMove=0;
        private void GameArea_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            condition = true;
            point = e.GetPosition(GameArea);
            GameArea.CaptureMouse();
            txtblock.Text = "";
            notFound = true;
            recentMove=Convert.ToInt32(TextBlockMove.Text.Substring(7));
            recentMove++;
            TextBlockMove.Text =  TextBlockMove.Text.Substring (0,7) + recentMove.ToString();
        }

        private void GameArea_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            double oldX = 0, oldY = 0, newX = 0, newY = 0, differenceX = 0, differenceY = 0;
            if (condition)
            {
                oldX = point.X;
                oldY = point.Y;
                point = e.GetPosition(GameArea);
                newX = point.X;
                newY = point.Y;
                differenceX = newX - oldX;
                differenceY = newY - oldY;
                differenceX = (differenceX < 0) ? (differenceX * (-1)) : differenceX;
                differenceY = (differenceY < 0) ? (differenceY * (-1)) : differenceY;
                if (differenceX > differenceY)
                {
                    if ((newX - oldX) > 0)
                    {
                        txtblock.Text = "Right";
                        Right();
                    }
                    else
                    {
                        txtblock.Text = "Left";
                        Left();
                    }
                }
                else
                {
                    if ((newY - oldY) > 0)
                    {
                        txtblock.Text = "Down";
                        Down();
                    }
                    else
                    {
                        txtblock.Text = "Up";
                        Up();
                    }
                }
            }
            condition = false;
            GameArea.ReleaseMouseCapture();
            int nextNumber;
            Random rand = new Random();
            while (notFound)
            {
                if (controlOfGameArea())
                {
                    if (isGameOver())
                    {
                       
                        while (notSaved)
                        {
                            try
                            {
                                StreamReader reader = new StreamReader("./BestScore.txt");
                                string bestScore = reader.ReadLine();
                                reader.Close();
                                if (findTheScore() > Convert.ToInt32(bestScore))
                                {
                                    StreamWriter writer = new StreamWriter("./BestScore.txt");
                                    writer.WriteLine(findTheScore().ToString());
                                    writer.Close();
                                }
                                notSaved = false;
                                MessageBox.Show("Done4");
                            }
                            catch (Exception)
                            {
                                notSaved = true;
                                MessageBox.Show("error");
                            }
                        }
                        MessageBox.Show("Congratulations! Your score : " + findTheScore().ToString(), "GAME OVER", MessageBoxButton.OK);
                        NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                    }
                    break;// this means "Do not produce a new number 2". Because there is no empty rectangle to write a new number 2
                }
                nextNumber = rand.Next(1, 17);
                if (TextB1.Name.Substring(5) == nextNumber.ToString() && TextB1.Text == "")
                {
                    TextB1.Text = "2"; notFound = false;
                }
                if (TextB2.Name.Substring(5) == nextNumber.ToString() && TextB2.Text == "")
                {
                    TextB2.Text = "2"; notFound = false;
                }
                if (TextB3.Name.Substring(5) == nextNumber.ToString() && TextB3.Text == "")
                {
                    TextB3.Text = "2"; notFound = false;
                }
                if (TextB4.Name.Substring(5) == nextNumber.ToString() && TextB4.Text == "")
                {
                    TextB4.Text = "2"; notFound = false;
                }
                if (TextB5.Name.Substring(5) == nextNumber.ToString() && TextB5.Text == "")
                {
                    TextB5.Text = "2"; notFound = false;
                }
                if (TextB6.Name.Substring(5) == nextNumber.ToString() && TextB6.Text == "")
                {
                    TextB6.Text = "2"; notFound = false;
                }
                if (TextB7.Name.Substring(5) == nextNumber.ToString() && TextB7.Text == "")
                {
                    TextB7.Text = "2"; notFound = false;
                }
                if (TextB8.Name.Substring(5) == nextNumber.ToString() && TextB8.Text == "")
                {
                    TextB8.Text = "2"; notFound = false;
                }
                if (TextB9.Name.Substring(5) == nextNumber.ToString() && TextB9.Text == "")
                {
                    TextB9.Text = "2"; notFound = false;
                }
                if (TextB10.Name.Substring(5) == nextNumber.ToString() && TextB10.Text == "")
                {
                    TextB10.Text = "2"; notFound = false;
                }
                if (TextB11.Name.Substring(5) == nextNumber.ToString() && TextB11.Text == "")
                {
                    TextB11.Text = "2"; notFound = false;
                }
                if (TextB12.Name.Substring(5) == nextNumber.ToString() && TextB12.Text == "")
                {
                    TextB12.Text = "2"; notFound = false;
                }
                if (TextB13.Name.Substring(5) == nextNumber.ToString() && TextB13.Text == "")
                {
                    TextB13.Text = "2"; notFound = false;
                }
                if (TextB14.Name.Substring(5) == nextNumber.ToString() && TextB14.Text == "")
                {
                    TextB14.Text = "2"; notFound = false;
                }
                if (TextB15.Name.Substring(5) == nextNumber.ToString() && TextB15.Text == "")
                {
                    TextB15.Text = "2"; notFound = false;
                }
                if (TextB16.Name.Substring(5) == nextNumber.ToString() && TextB16.Text == "")
                {
                    TextB16.Text = "2"; notFound = false;
                }
            }    
        }

        public int findTheScore()
        {
            int theScore=0;
            int[] values = { 0, Convert.ToInt32(TextB1.Text), Convert.ToInt32(TextB2.Text), Convert.ToInt32(TextB3.Text), Convert.ToInt32(TextB4.Text),
                           Convert.ToInt32(TextB5.Text), Convert.ToInt32(TextB6.Text), Convert.ToInt32(TextB7.Text), Convert.ToInt32(TextB8.Text),
                           Convert.ToInt32(TextB9.Text), Convert.ToInt32(TextB10.Text), Convert.ToInt32(TextB11.Text), Convert.ToInt32(TextB12.Text),
                           Convert.ToInt32(TextB13.Text), Convert.ToInt32(TextB14.Text), Convert.ToInt32(TextB15.Text), Convert.ToInt32(TextB16.Text)};
            for (int i = 1; i <= 16; i++)
            {
                if (values[i] > theScore)
                {
                    theScore = values[i];
                }
            }
                return theScore;
        }

        public bool isGameOver()
        {
            if(TextB1.Text != TextB5.Text && TextB5.Text != TextB9.Text && TextB9.Text != TextB13.Text &&
                TextB1.Text != TextB2.Text && TextB5.Text != TextB6.Text && TextB9.Text != TextB10.Text && TextB13.Text != TextB14.Text &&
                TextB2.Text != TextB6.Text && TextB6.Text != TextB10.Text && TextB10.Text != TextB14.Text &&
                TextB2.Text != TextB3.Text && TextB6.Text != TextB7.Text && TextB10.Text != TextB11.Text && TextB14.Text != TextB15.Text &&
                TextB3.Text != TextB7.Text && TextB7.Text != TextB11.Text && TextB11.Text != TextB15.Text &&
                TextB3.Text != TextB4.Text && TextB7.Text != TextB8.Text && TextB11.Text != TextB12.Text && TextB15.Text != TextB16.Text &&
                TextB4.Text != TextB8.Text && TextB8.Text != TextB12.Text && TextB12.Text != TextB16.Text)
            {
                return true;
            }
            return false;
        }
        public bool controlOfGameArea()// if this method returns the value "true", it means "No empty rectangle on the game area"
        {
            if (TextB1.Text != "")
            {
                if (TextB2.Text != "")
                {
                    if (TextB3.Text != "")
                    {
                        if (TextB4.Text != "")
                        {
                            if (TextB5.Text != "")
                            {
                                if (TextB6.Text != "")
                                {
                                    if (TextB7.Text != "")
                                    {
                                        if (TextB8.Text != "")
                                        {
                                            if (TextB9.Text != "")
                                            {
                                                if (TextB10.Text != "")
                                                {
                                                    if (TextB11.Text != "")
                                                    {
                                                        if (TextB12.Text != "")
                                                        {
                                                            if (TextB13.Text != "")
                                                            {
                                                                if (TextB14.Text != "")
                                                                {
                                                                    if (TextB15.Text != "")
                                                                    {
                                                                        if (TextB16.Text != "")
                                                                        {
                                                                            return true;
                                                                        }
                                                                        return false;
                                                                    }
                                                                    return false;
                                                                }
                                                                return false;
                                                            }
                                                            return false;
                                                        }
                                                        return false;
                                                    }
                                                    return false;
                                                }
                                                return false;
                                            }
                                            return false;
                                        }
                                        return false;
                                    }
                                    return false;
                                }
                                return false;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;

            
        }
        private void GameArea_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            
        }

        public void Right()
        {
            int sum;
            //*****First Line******
            if (TextB9.Text != "")
            {
                if (TextB9.Text == TextB13.Text)
                {
                    sum = Convert.ToInt32(TextB13.Text);
                    sum *= 2; TextB13.Text = sum.ToString();
                    TextB9.Text = "";
                }
                else if (TextB13.Text == "")
                {
                    TextB13.Text = TextB9.Text;
                    TextB9.Text = "";
                }
            }
            //----------------------------------
            if (TextB5.Text != "")
            {
                if (TextB5.Text == TextB9.Text)
                {
                    sum = Convert.ToInt32(TextB9.Text);
                    sum *= 2; TextB9.Text = sum.ToString();
                    TextB5.Text = "";
                }
                else if (TextB9.Text == "")
                {
                    if (TextB5.Text == TextB13.Text)
                    {
                        sum = Convert.ToInt32(TextB13.Text);
                        sum *= 2; TextB13.Text = sum.ToString();
                        TextB5.Text = "";
                    }
                    else if (TextB13.Text == "")
                    {
                        TextB13.Text = TextB5.Text;
                        TextB5.Text = "";
                    }
                    else
                    {
                        TextB9.Text = TextB5.Text;
                        TextB5.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB1.Text != "")
            {
                if (TextB1.Text == TextB5.Text)
                {
                    sum = Convert.ToInt32(TextB5.Text);
                    sum *= 2; TextB5.Text = sum.ToString();
                    TextB1.Text = "";
                }
                else if (TextB5.Text == "")
                {
                    if (TextB1.Text == TextB9.Text)
                    {
                        sum = Convert.ToInt32(TextB9.Text);
                        sum *= 2; TextB9.Text = sum.ToString();
                        TextB1.Text = "";
                    }
                    else if (TextB9.Text == "")
                    {
                        if (TextB13.Text == "")
                        {
                            TextB13.Text = TextB1.Text;
                            TextB1.Text = "";
                        }
                        else if (TextB13.Text == TextB1.Text)
                        {
                            sum = Convert.ToInt32(TextB13.Text);
                            sum *= 2; TextB13.Text = sum.ToString();
                            TextB1.Text = "";
                        }
                        else
                        {
                            TextB9.Text = TextB1.Text;
                            TextB1.Text = "";
                        }
                    }
                    else
                    {
                        TextB5.Text = TextB1.Text;
                        TextB1.Text = "";
                    }

                }
            }
            //******End of first line*******
            //******Second Line*******
            if (TextB10.Text != "")
            {
                if (TextB10.Text == TextB14.Text)
                {
                    sum = Convert.ToInt32(TextB14.Text);
                    sum *= 2; TextB14.Text = sum.ToString();
                    TextB10.Text = "";
                }
                else if (TextB14.Text == "")
                {
                    TextB14.Text = TextB10.Text;
                    TextB10.Text = "";
                }
            }
            //----------------------------------
            if (TextB6.Text != "")
            {
                if (TextB6.Text == TextB10.Text)
                {
                    sum = Convert.ToInt32(TextB10.Text);
                    sum *= 2; TextB10.Text = sum.ToString();
                    TextB6.Text = "";
                }
                else if (TextB10.Text == "")
                {
                    if (TextB6.Text == TextB14.Text)
                    {
                        sum = Convert.ToInt32(TextB14.Text);
                        sum *= 2; TextB14.Text = sum.ToString();
                        TextB6.Text = "";
                    }
                    else if (TextB14.Text == "")
                    {
                        TextB14.Text = TextB6.Text;
                        TextB6.Text = "";
                    }
                    else
                    {
                        TextB10.Text = TextB6.Text;
                        TextB6.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB2.Text != "")
            {
                if (TextB2.Text == TextB6.Text)
                {
                    sum = Convert.ToInt32(TextB6.Text);
                    sum *= 2; TextB6.Text = sum.ToString();
                    TextB2.Text = "";
                }
                else if (TextB6.Text == "")
                {
                    if (TextB2.Text == TextB10.Text)
                    {
                        sum = Convert.ToInt32(TextB10.Text);
                        sum *= 2; TextB10.Text = sum.ToString();
                        TextB2.Text = "";
                    }
                    else if (TextB10.Text == "")
                    {
                        if (TextB14.Text == "")
                        {
                            TextB14.Text = TextB2.Text;
                            TextB2.Text = "";
                        }
                        else if (TextB14.Text == TextB2.Text)
                        {
                            sum = Convert.ToInt32(TextB14.Text);
                            sum *= 2; TextB14.Text = sum.ToString();
                            TextB2.Text = "";
                        }
                        else
                        {
                            TextB10.Text = TextB2.Text;
                            TextB2.Text = "";
                        }
                    }
                    else
                    {
                        TextB6.Text = TextB2.Text;
                        TextB2.Text = "";
                    }

                }
            }
            //******End of second line******
            //******Third Line*******
            if (TextB11.Text != "")
            {
                if (TextB11.Text == TextB15.Text)
                {
                    sum = Convert.ToInt32(TextB15.Text);
                    sum *= 2; TextB15.Text = sum.ToString();
                    TextB11.Text = "";
                }
                else if (TextB15.Text == "")
                {
                    TextB15.Text = TextB11.Text;
                    TextB11.Text = "";
                }
            }
            //----------------------------------
            if (TextB7.Text != "")
            {
                if (TextB7.Text == TextB11.Text)
                {
                    sum = Convert.ToInt32(TextB11.Text);
                    sum *= 2; TextB11.Text = sum.ToString();
                    TextB7.Text = "";
                }
                else if (TextB11.Text == "")
                {
                    if (TextB7.Text == TextB15.Text)
                    {
                        sum = Convert.ToInt32(TextB15.Text);
                        sum *= 2; TextB15.Text = sum.ToString();
                        TextB7.Text = "";
                    }
                    else if (TextB15.Text == "")
                    {
                        TextB15.Text = TextB7.Text;
                        TextB7.Text = "";
                    }
                    else
                    {
                        TextB11.Text = TextB7.Text;
                        TextB7.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB3.Text != "")
            {
                if (TextB3.Text == TextB7.Text)
                {
                    sum = Convert.ToInt32(TextB7.Text);
                    sum *= 2; TextB7.Text = sum.ToString();
                    TextB3.Text = "";
                }
                else if (TextB7.Text == "")
                {
                    if (TextB3.Text == TextB11.Text)
                    {
                        sum = Convert.ToInt32(TextB11.Text);
                        sum *= 2; TextB11.Text = sum.ToString();
                        TextB3.Text = "";
                    }
                    else if (TextB11.Text == "")
                    {
                        if (TextB15.Text == "")
                        {
                            TextB15.Text = TextB3.Text;
                            TextB3.Text = "";
                        }
                        else if (TextB15.Text == TextB3.Text)
                        {
                            sum = Convert.ToInt32(TextB15.Text);
                            sum *= 2; TextB15.Text = sum.ToString();
                            TextB3.Text = "";
                        }
                        else
                        {
                            TextB11.Text = TextB3.Text;
                            TextB3.Text = "";
                        }
                    }
                    else
                    {
                        TextB7.Text = TextB3.Text;
                        TextB3.Text = "";
                    }

                }
            }
            //******End of third line*******
            //******Fourth Line*******
            if (TextB12.Text != "")
            {
                if (TextB12.Text == TextB16.Text)
                {
                    sum = Convert.ToInt32(TextB16.Text);
                    sum *= 2; TextB16.Text = sum.ToString();
                    TextB12.Text = "";
                }
                else if (TextB16.Text == "")
                {
                    TextB16.Text = TextB12.Text;
                    TextB12.Text = "";
                }
            }
            //----------------------------------
            if (TextB8.Text != "")
            {
                if (TextB8.Text == TextB12.Text)
                {
                    sum = Convert.ToInt32(TextB12.Text);
                    sum *= 2; TextB12.Text = sum.ToString();
                    TextB8.Text = "";
                }
                else if (TextB12.Text == "")
                {
                    if (TextB8.Text == TextB16.Text)
                    {
                        sum = Convert.ToInt32(TextB16.Text);
                        sum *= 2; TextB16.Text = sum.ToString();
                        TextB8.Text = "";
                    }
                    else if (TextB16.Text == "")
                    {
                        TextB16.Text = TextB8.Text;
                        TextB8.Text = "";
                    }
                    else
                    {
                        TextB12.Text = TextB8.Text;
                        TextB8.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB4.Text != "")
            {
                if (TextB4.Text == TextB8.Text)
                {
                    sum = Convert.ToInt32(TextB8.Text);
                    sum *= 2; TextB8.Text = sum.ToString();
                    TextB4.Text = "";
                }
                else if (TextB8.Text == "")
                {
                    if (TextB4.Text == TextB12.Text)
                    {
                        sum = Convert.ToInt32(TextB12.Text);
                        sum *= 2; TextB12.Text = sum.ToString();
                        TextB4.Text = "";
                    }
                    else if (TextB12.Text == "")
                    {
                        if (TextB16.Text == "")
                        {
                            TextB16.Text = TextB4.Text;
                            TextB4.Text = "";
                        }
                        else if (TextB16.Text == TextB4.Text)
                        {
                            sum = Convert.ToInt32(TextB16.Text);
                            sum *= 2; TextB16.Text = sum.ToString();
                            TextB4.Text = "";
                        }
                        else
                        {
                            TextB12.Text = TextB4.Text;
                            TextB4.Text = "";
                        }
                    }
                    else
                    {
                        TextB8.Text = TextB4.Text;
                        TextB4.Text = "";
                    }

                }
            }
            //******End of fourth line******
        }

        public void Left()
        {
            int sum;
            //*****First Line******
            if (TextB8.Text != "")
            {
                if (TextB8.Text == TextB4.Text)
                {
                    sum = Convert.ToInt32(TextB4.Text);
                    sum *= 2; TextB4.Text = sum.ToString();
                    TextB8.Text = "";
                }
                else if (TextB4.Text == "")
                {
                    TextB4.Text = TextB8.Text;
                    TextB8.Text = "";
                }
            }
            //----------------------------------
            if (TextB12.Text != "")
            {
                if (TextB12.Text == TextB8.Text)
                {
                    sum = Convert.ToInt32(TextB8.Text);
                    sum *= 2; TextB8.Text = sum.ToString();
                    TextB12.Text = "";
                }
                else if (TextB8.Text == "")
                {
                    if (TextB12.Text == TextB4.Text)
                    {
                        sum = Convert.ToInt32(TextB4.Text);
                        sum *= 2; TextB4.Text = sum.ToString();
                        TextB12.Text = "";
                    }
                    else if (TextB4.Text == "")
                    {
                        TextB4.Text = TextB12.Text;
                        TextB12.Text = "";
                    }
                    else
                    {
                        TextB8.Text = TextB12.Text;
                        TextB12.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB16.Text != "")
            {
                if (TextB16.Text == TextB12.Text)
                {
                    sum = Convert.ToInt32(TextB12.Text);
                    sum *= 2; TextB12.Text = sum.ToString();
                    TextB16.Text = "";
                }
                else if (TextB12.Text == "")
                {
                    if (TextB16.Text == TextB8.Text)
                    {
                        sum = Convert.ToInt32(TextB8.Text);
                        sum *= 2; TextB8.Text = sum.ToString();
                        TextB16.Text = "";
                    }
                    else if (TextB8.Text == "")
                    {
                        if (TextB4.Text == "")
                        {
                            TextB4.Text = TextB16.Text;
                            TextB16.Text = "";
                        }
                        else if (TextB4.Text == TextB16.Text)
                        {
                            sum = Convert.ToInt32(TextB4.Text);
                            sum *= 2; TextB4.Text = sum.ToString();
                            TextB16.Text = "";
                        }
                        else
                        {
                            TextB8.Text = TextB16.Text;
                            TextB16.Text = "";
                        }
                    }
                    else
                    {
                        TextB12.Text = TextB16.Text;
                        TextB16.Text = "";
                    }

                }
            }
            //******End of first line*******
            //*****Second Line******
            if (TextB7.Text != "")
            {
                if (TextB7.Text == TextB3.Text)
                {
                    sum = Convert.ToInt32(TextB3.Text);
                    sum *= 2; TextB3.Text = sum.ToString();
                    TextB7.Text = "";
                }
                else if (TextB3.Text == "")
                {
                    TextB3.Text = TextB7.Text;
                    TextB7.Text = "";
                }
            }
            //----------------------------------
            if (TextB11.Text != "")
            {
                if (TextB11.Text == TextB7.Text)
                {
                    sum = Convert.ToInt32(TextB7.Text);
                    sum *= 2; TextB7.Text = sum.ToString();
                    TextB11.Text = "";
                }
                else if (TextB7.Text == "")
                {
                    if (TextB11.Text == TextB3.Text)
                    {
                        sum = Convert.ToInt32(TextB3.Text);
                        sum *= 2; TextB3.Text = sum.ToString();
                        TextB11.Text = "";
                    }
                    else if (TextB3.Text == "")
                    {
                        TextB3.Text = TextB11.Text;
                        TextB11.Text = "";
                    }
                    else
                    {
                        TextB7.Text = TextB11.Text;
                        TextB11.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB15.Text != "")
            {
                if (TextB15.Text == TextB11.Text)
                {
                    sum = Convert.ToInt32(TextB11.Text);
                    sum *= 2; TextB11.Text = sum.ToString();
                    TextB15.Text = "";
                }
                else if (TextB11.Text == "")
                {
                    if (TextB15.Text == TextB7.Text)
                    {
                        sum = Convert.ToInt32(TextB7.Text);
                        sum *= 2; TextB7.Text = sum.ToString();
                        TextB15.Text = "";
                    }
                    else if (TextB7.Text == "")
                    {
                        if (TextB3.Text == "")
                        {
                            TextB3.Text = TextB15.Text;
                            TextB15.Text = "";
                        }
                        else if (TextB3.Text == TextB15.Text)
                        {
                            sum = Convert.ToInt32(TextB3.Text);
                            sum *= 2; TextB3.Text = sum.ToString();
                            TextB15.Text = "";
                        }
                        else
                        {
                            TextB7.Text = TextB15.Text;
                            TextB15.Text = "";
                        }
                    }
                    else
                    {
                        TextB11.Text = TextB15.Text;
                        TextB15.Text = "";
                    }

                }
            }
            //******End of second line*******
            //*****Third Line******
            if (TextB6.Text != "")
            {
                if (TextB6.Text == TextB2.Text)
                {
                    sum = Convert.ToInt32(TextB2.Text);
                    sum *= 2; TextB2.Text = sum.ToString();
                    TextB6.Text = "";
                }
                else if (TextB2.Text == "")
                {
                    TextB2.Text = TextB6.Text;
                    TextB6.Text = "";
                }
            }
            //----------------------------------
            if (TextB10.Text != "")
            {
                if (TextB10.Text == TextB6.Text)
                {
                    sum = Convert.ToInt32(TextB6.Text);
                    sum *= 2; TextB6.Text = sum.ToString();
                    TextB10.Text = "";
                }
                else if (TextB6.Text == "")
                {
                    if (TextB10.Text == TextB2.Text)
                    {
                        sum = Convert.ToInt32(TextB2.Text);
                        sum *= 2; TextB2.Text = sum.ToString();
                        TextB10.Text = "";
                    }
                    else if (TextB2.Text == "")
                    {
                        TextB2.Text = TextB10.Text;
                        TextB10.Text = "";
                    }
                    else
                    {
                        TextB6.Text = TextB10.Text;
                        TextB10.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB14.Text != "")
            {
                if (TextB14.Text == TextB10.Text)
                {
                    sum = Convert.ToInt32(TextB10.Text);
                    sum *= 2; TextB10.Text = sum.ToString();
                    TextB14.Text = "";
                }
                else if (TextB10.Text == "")
                {
                    if (TextB14.Text == TextB6.Text)
                    {
                        sum = Convert.ToInt32(TextB6.Text);
                        sum *= 2; TextB6.Text = sum.ToString();
                        TextB14.Text = "";
                    }
                    else if (TextB6.Text == "")
                    {
                        if (TextB2.Text == "")
                        {
                            TextB2.Text = TextB14.Text;
                            TextB14.Text = "";
                        }
                        else if (TextB2.Text == TextB14.Text)
                        {
                            sum = Convert.ToInt32(TextB2.Text);
                            sum *= 2; TextB2.Text = sum.ToString();
                            TextB14.Text = "";
                        }
                        else
                        {
                            TextB6.Text = TextB14.Text;
                            TextB14.Text = "";
                        }
                    }
                    else
                    {
                        TextB10.Text = TextB14.Text;
                        TextB14.Text = "";
                    }

                }
            }
            //******End of third line*******
            //*****Fourth Line******
            if (TextB5.Text != "")
            {
                if (TextB5.Text == TextB1.Text)
                {
                    sum = Convert.ToInt32(TextB1.Text);
                    sum *= 2; TextB1.Text = sum.ToString();
                    TextB5.Text = "";
                }
                else if (TextB1.Text == "")
                {
                    TextB1.Text = TextB5.Text;
                    TextB5.Text = "";
                }
            }
            //----------------------------------
            if (TextB9.Text != "")
            {
                if (TextB9.Text == TextB5.Text)
                {
                    sum = Convert.ToInt32(TextB5.Text);
                    sum *= 2; TextB5.Text = sum.ToString();
                    TextB9.Text = "";
                }
                else if (TextB5.Text == "")
                {
                    if (TextB9.Text == TextB1.Text)
                    {
                        sum = Convert.ToInt32(TextB1.Text);
                        sum *= 2; TextB1.Text = sum.ToString();
                        TextB9.Text = "";
                    }
                    else if (TextB1.Text == "")
                    {
                        TextB1.Text = TextB9.Text;
                        TextB9.Text = "";
                    }
                    else
                    {
                        TextB5.Text = TextB9.Text;
                        TextB9.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB13.Text != "")
            {
                if (TextB13.Text == TextB9.Text)
                {
                    sum = Convert.ToInt32(TextB9.Text);
                    sum *= 2; TextB9.Text = sum.ToString();
                    TextB13.Text = "";
                }
                else if (TextB9.Text == "")
                {
                    if (TextB13.Text == TextB5.Text)
                    {
                        sum = Convert.ToInt32(TextB5.Text);
                        sum *= 2; TextB5.Text = sum.ToString();
                        TextB13.Text = "";
                    }
                    else if (TextB5.Text == "")
                    {
                        if (TextB1.Text == "")
                        {
                            TextB1.Text = TextB13.Text;
                            TextB13.Text = "";
                        }
                        else if (TextB1.Text == TextB13.Text)
                        {
                            sum = Convert.ToInt32(TextB1.Text);
                            sum *= 2; TextB1.Text = sum.ToString();
                            TextB13.Text = "";
                        }
                        else
                        {
                            TextB5.Text = TextB13.Text;
                            TextB13.Text = "";
                        }
                    }
                    else
                    {
                        TextB9.Text = TextB13.Text;
                        TextB13.Text = "";
                    }

                }
            }
            //******End of fourth line*******
        }
        public void Down()
        {
            int sum;
            //*****First Line******
            if (TextB15.Text != "")
            {
                if (TextB15.Text == TextB16.Text)
                {
                    sum = Convert.ToInt32(TextB16.Text);
                    sum *= 2; TextB16.Text = sum.ToString();
                    TextB15.Text = "";
                }
                else if (TextB16.Text == "")
                {
                    TextB16.Text = TextB15.Text;
                    TextB15.Text = "";
                }
            }
            //----------------------------------
            if (TextB14.Text != "")
            {
                if (TextB14.Text == TextB15.Text)
                {
                    sum = Convert.ToInt32(TextB15.Text);
                    sum *= 2; TextB15.Text = sum.ToString();
                    TextB14.Text = "";
                }
                else if (TextB15.Text == "")
                {
                    if (TextB14.Text == TextB16.Text)
                    {
                        sum = Convert.ToInt32(TextB16.Text);
                        sum *= 2; TextB16.Text = sum.ToString();
                        TextB14.Text = "";
                    }
                    else if (TextB16.Text == "")
                    {
                        TextB16.Text = TextB14.Text;
                        TextB14.Text = "";
                    }
                    else
                    {
                        TextB15.Text = TextB14.Text;
                        TextB14.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB13.Text != "")
            {
                if (TextB13.Text == TextB14.Text)
                {
                    sum = Convert.ToInt32(TextB14.Text);
                    sum *= 2; TextB14.Text = sum.ToString();
                    TextB13.Text = "";
                }
                else if (TextB14.Text == "")
                {
                    if (TextB13.Text == TextB15.Text)
                    {
                        sum = Convert.ToInt32(TextB15.Text);
                        sum *= 2; TextB15.Text = sum.ToString();
                        TextB13.Text = "";
                    }
                    else if (TextB15.Text == "")
                    {
                        if (TextB16.Text == "")
                        {
                            TextB16.Text = TextB13.Text;
                            TextB13.Text = "";
                        }
                        else if (TextB16.Text == TextB13.Text)
                        {
                            sum = Convert.ToInt32(TextB16.Text);
                            sum *= 2; TextB16.Text = sum.ToString();
                            TextB13.Text = "";
                        }
                        else
                        {
                            TextB15.Text = TextB13.Text;
                            TextB13.Text = "";
                        }
                    }
                    else
                    {
                        TextB14.Text = TextB13.Text;
                        TextB13.Text = "";
                    }

                }
            }
            //******End of first line*******
            //*****Second Line******
            if (TextB11.Text != "")
            {
                if (TextB11.Text == TextB12.Text)
                {
                    sum = Convert.ToInt32(TextB12.Text);
                    sum *= 2; TextB12.Text = sum.ToString();
                    TextB11.Text = "";
                }
                else if (TextB12.Text == "")
                {
                    TextB12.Text = TextB11.Text;
                    TextB11.Text = "";
                }
            }
            //----------------------------------
            if (TextB10.Text != "")
            {
                if (TextB10.Text == TextB11.Text)
                {
                    sum = Convert.ToInt32(TextB11.Text);
                    sum *= 2; TextB11.Text = sum.ToString();
                    TextB10.Text = "";
                }
                else if (TextB11.Text == "")
                {
                    if (TextB10.Text == TextB12.Text)
                    {
                        sum = Convert.ToInt32(TextB12.Text);
                        sum *= 2; TextB12.Text = sum.ToString();
                        TextB10.Text = "";
                    }
                    else if (TextB12.Text == "")
                    {
                        TextB12.Text = TextB10.Text;
                        TextB10.Text = "";
                    }
                    else
                    {
                        TextB11.Text = TextB10.Text;
                        TextB10.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB9.Text != "")
            {
                if (TextB9.Text == TextB10.Text)
                {
                    sum = Convert.ToInt32(TextB10.Text);
                    sum *= 2; TextB10.Text = sum.ToString();
                    TextB9.Text = "";
                }
                else if (TextB10.Text == "")
                {
                    if (TextB9.Text == TextB11.Text)
                    {
                        sum = Convert.ToInt32(TextB11.Text);
                        sum *= 2; TextB11.Text = sum.ToString();
                        TextB9.Text = "";
                    }
                    else if (TextB11.Text == "")
                    {
                        if (TextB12.Text == "")
                        {
                            TextB12.Text = TextB9.Text;
                            TextB9.Text = "";
                        }
                        else if (TextB12.Text == TextB9.Text)
                        {
                            sum = Convert.ToInt32(TextB12.Text);
                            sum *= 2; TextB12.Text = sum.ToString();
                            TextB9.Text = "";
                        }
                        else
                        {
                            TextB11.Text = TextB9.Text;
                            TextB9.Text = "";
                        }
                    }
                    else
                    {
                        TextB10.Text = TextB9.Text;
                        TextB9.Text = "";
                    }

                }
            }
            //******End of second line*******
            //*****Third Line******
            if (TextB7.Text != "")
            {
                if (TextB7.Text == TextB8.Text)
                {
                    sum = Convert.ToInt32(TextB8.Text);
                    sum *= 2; TextB8.Text = sum.ToString();
                    TextB7.Text = "";
                }
                else if (TextB8.Text == "")
                {
                    TextB8.Text = TextB7.Text;
                    TextB7.Text = "";
                }
            }
            //----------------------------------
            if (TextB6.Text != "")
            {
                if (TextB6.Text == TextB7.Text)
                {
                    sum = Convert.ToInt32(TextB7.Text);
                    sum *= 2; TextB7.Text = sum.ToString();
                    TextB6.Text = "";
                }
                else if (TextB7.Text == "")
                {
                    if (TextB6.Text == TextB8.Text)
                    {
                        sum = Convert.ToInt32(TextB8.Text);
                        sum *= 2; TextB8.Text = sum.ToString();
                        TextB6.Text = "";
                    }
                    else if (TextB8.Text == "")
                    {
                        TextB8.Text = TextB6.Text;
                        TextB6.Text = "";
                    }
                    else
                    {
                        TextB7.Text = TextB6.Text;
                        TextB6.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB5.Text != "")
            {
                if (TextB5.Text == TextB6.Text)
                {
                    sum = Convert.ToInt32(TextB6.Text);
                    sum *= 2; TextB6.Text = sum.ToString();
                    TextB5.Text = "";
                }
                else if (TextB6.Text == "")
                {
                    if (TextB5.Text == TextB7.Text)
                    {
                        sum = Convert.ToInt32(TextB7.Text);
                        sum *= 2; TextB7.Text = sum.ToString();
                        TextB5.Text = "";
                    }
                    else if (TextB7.Text == "")
                    {
                        if (TextB8.Text == "")
                        {
                            TextB8.Text = TextB5.Text;
                            TextB5.Text = "";
                        }
                        else if (TextB8.Text == TextB5.Text)
                        {
                            sum = Convert.ToInt32(TextB8.Text);
                            sum *= 2; TextB8.Text = sum.ToString();
                            TextB5.Text = "";
                        }
                        else
                        {
                            TextB7.Text = TextB5.Text;
                            TextB5.Text = "";
                        }
                    }
                    else
                    {
                        TextB6.Text = TextB5.Text;
                        TextB5.Text = "";
                    }

                }
            }
            //******End of third line*******
            //*****Fourth Line******
            if (TextB3.Text != "")
            {
                if (TextB3.Text == TextB4.Text)
                {
                    sum = Convert.ToInt32(TextB4.Text);
                    sum *= 2; TextB4.Text = sum.ToString();
                    TextB3.Text = "";
                }
                else if (TextB4.Text == "")
                {
                    TextB4.Text = TextB3.Text;
                    TextB3.Text = "";
                }
            }
            //----------------------------------
            if (TextB2.Text != "")
            {
                if (TextB2.Text == TextB3.Text)
                {
                    sum = Convert.ToInt32(TextB3.Text);
                    sum *= 2; TextB3.Text = sum.ToString();
                    TextB2.Text = "";
                }
                else if (TextB3.Text == "")
                {
                    if (TextB2.Text == TextB4.Text)
                    {
                        sum = Convert.ToInt32(TextB4.Text);
                        sum *= 2; TextB4.Text = sum.ToString();
                        TextB2.Text = "";
                    }
                    else if (TextB4.Text == "")
                    {
                        TextB4.Text = TextB2.Text;
                        TextB2.Text = "";
                    }
                    else
                    {
                        TextB3.Text = TextB2.Text;
                        TextB2.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB1.Text != "")
            {
                if (TextB1.Text == TextB2.Text)
                {
                    sum = Convert.ToInt32(TextB2.Text);
                    sum *= 2; TextB2.Text = sum.ToString();
                    TextB1.Text = "";
                }
                else if (TextB2.Text == "")
                {
                    if (TextB1.Text == TextB3.Text)
                    {
                        sum = Convert.ToInt32(TextB3.Text);
                        sum *= 2; TextB3.Text = sum.ToString();
                        TextB1.Text = "";
                    }
                    else if (TextB3.Text == "")
                    {
                        if (TextB4.Text == "")
                        {
                            TextB4.Text = TextB1.Text;
                            TextB1.Text = "";
                        }
                        else if (TextB4.Text == TextB1.Text)
                        {
                            sum = Convert.ToInt32(TextB4.Text);
                            sum *= 2; TextB4.Text = sum.ToString();
                            TextB1.Text = "";
                        }
                        else
                        {
                            TextB3.Text = TextB1.Text;
                            TextB1.Text = "";
                        }
                    }
                    else
                    {
                        TextB2.Text = TextB1.Text;
                        TextB1.Text = "";
                    }

                }
            }
            //******End of fourth line*******
        }
        public void Up()
        {
            int sum;
            //*****First Line******
            if (TextB2.Text != "")
            {
                if (TextB2.Text == TextB1.Text)
                {
                    sum = Convert.ToInt32(TextB1.Text);
                    sum *= 2; TextB1.Text = sum.ToString();
                    TextB2.Text = "";
                }
                else if (TextB1.Text == "")
                {
                    TextB1.Text = TextB2.Text;
                    TextB2.Text = "";
                }
            }
            //----------------------------------
            if (TextB3.Text != "")
            {
                if (TextB3.Text == TextB2.Text)
                {
                    sum = Convert.ToInt32(TextB2.Text);
                    sum *= 2; TextB2.Text = sum.ToString();
                    TextB3.Text = "";
                }
                else if (TextB2.Text == "")
                {
                    if (TextB3.Text == TextB1.Text)
                    {
                        sum = Convert.ToInt32(TextB1.Text);
                        sum *= 2; TextB1.Text = sum.ToString();
                        TextB3.Text = "";
                    }
                    else if (TextB1.Text == "")
                    {
                        TextB1.Text = TextB3.Text;
                        TextB3.Text = "";
                    }
                    else
                    {
                        TextB2.Text = TextB3.Text;
                        TextB3.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB4.Text != "")
            {
                if (TextB4.Text == TextB3.Text)
                {
                    sum = Convert.ToInt32(TextB3.Text);
                    sum *= 2; TextB3.Text = sum.ToString();
                    TextB4.Text = "";
                }
                else if (TextB3.Text == "")
                {
                    if (TextB4.Text == TextB2.Text)
                    {
                        sum = Convert.ToInt32(TextB2.Text);
                        sum *= 2; TextB2.Text = sum.ToString();
                        TextB4.Text = "";
                    }
                    else if (TextB2.Text == "")
                    {
                        if (TextB1.Text == "")
                        {
                            TextB1.Text = TextB4.Text;
                            TextB4.Text = "";
                        }
                        else if (TextB1.Text == TextB4.Text)
                        {
                            sum = Convert.ToInt32(TextB1.Text);
                            sum *= 2; TextB1.Text = sum.ToString();
                            TextB4.Text = "";
                        }
                        else
                        {
                            TextB2.Text = TextB4.Text;
                            TextB4.Text = "";
                        }
                    }
                    else
                    {
                        TextB3.Text = TextB4.Text;
                        TextB4.Text = "";
                    }

                }
            }
            //******End of first line*******
            //*****Second Line******
            if (TextB6.Text != "")
            {
                if (TextB6.Text == TextB5.Text)
                {
                    sum = Convert.ToInt32(TextB5.Text);
                    sum *= 2; TextB5.Text = sum.ToString();
                    TextB6.Text = "";
                }
                else if (TextB5.Text == "")
                {
                    TextB5.Text = TextB6.Text;
                    TextB6.Text = "";
                }
            }
            //----------------------------------
            if (TextB7.Text != "")
            {
                if (TextB7.Text == TextB6.Text)
                {
                    sum = Convert.ToInt32(TextB6.Text);
                    sum *= 2; TextB6.Text = sum.ToString();
                    TextB7.Text = "";
                }
                else if (TextB6.Text == "")
                {
                    if (TextB7.Text == TextB5.Text)
                    {
                        sum = Convert.ToInt32(TextB5.Text);
                        sum *= 2; TextB5.Text = sum.ToString();
                        TextB7.Text = "";
                    }
                    else if (TextB5.Text == "")
                    {
                        TextB5.Text = TextB7.Text;
                        TextB7.Text = "";
                    }
                    else
                    {
                        TextB6.Text = TextB7.Text;
                        TextB7.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB8.Text != "")
            {
                if (TextB8.Text == TextB7.Text)
                {
                    sum = Convert.ToInt32(TextB7.Text);
                    sum *= 2; TextB7.Text = sum.ToString();
                    TextB8.Text = "";
                }
                else if (TextB7.Text == "")
                {
                    if (TextB8.Text == TextB6.Text)
                    {
                        sum = Convert.ToInt32(TextB6.Text);
                        sum *= 2; TextB6.Text = sum.ToString();
                        TextB8.Text = "";
                    }
                    else if (TextB6.Text == "")
                    {
                        if (TextB5.Text == "")
                        {
                            TextB5.Text = TextB8.Text;
                            TextB8.Text = "";
                        }
                        else if (TextB5.Text == TextB8.Text)
                        {
                            sum = Convert.ToInt32(TextB5.Text);
                            sum *= 2; TextB5.Text = sum.ToString();
                            TextB8.Text = "";
                        }
                        else
                        {
                            TextB6.Text = TextB8.Text;
                            TextB8.Text = "";
                        }
                    }
                    else
                    {
                        TextB7.Text = TextB8.Text;
                        TextB8.Text = "";
                    }

                }
            }
            //******End of second line*******
            //*****Third Line******
            if (TextB10.Text != "")
            {
                if (TextB10.Text == TextB9.Text)
                {
                    sum = Convert.ToInt32(TextB9.Text);
                    sum *= 2; TextB9.Text = sum.ToString();
                    TextB10.Text = "";
                }
                else if (TextB9.Text == "")
                {
                    TextB9.Text = TextB10.Text;
                    TextB10.Text = "";
                }
            }
            //----------------------------------
            if (TextB11.Text != "")
            {
                if (TextB11.Text == TextB10.Text)
                {
                    sum = Convert.ToInt32(TextB10.Text);
                    sum *= 2; TextB10.Text = sum.ToString();
                    TextB11.Text = "";
                }
                else if (TextB10.Text == "")
                {
                    if (TextB11.Text == TextB9.Text)
                    {
                        sum = Convert.ToInt32(TextB9.Text);
                        sum *= 2; TextB9.Text = sum.ToString();
                        TextB11.Text = "";
                    }
                    else if (TextB9.Text == "")
                    {
                        TextB9.Text = TextB11.Text;
                        TextB11.Text = "";
                    }
                    else
                    {
                        TextB10.Text = TextB11.Text;
                        TextB11.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB12.Text != "")
            {
                if (TextB12.Text == TextB11.Text)
                {
                    sum = Convert.ToInt32(TextB11.Text);
                    sum *= 2; TextB11.Text = sum.ToString();
                    TextB12.Text = "";
                }
                else if (TextB11.Text == "")
                {
                    if (TextB12.Text == TextB10.Text)
                    {
                        sum = Convert.ToInt32(TextB10.Text);
                        sum *= 2; TextB10.Text = sum.ToString();
                        TextB12.Text = "";
                    }
                    else if (TextB10.Text == "")
                    {
                        if (TextB9.Text == "")
                        {
                            TextB9.Text = TextB12.Text;
                            TextB12.Text = "";
                        }
                        else if (TextB9.Text == TextB12.Text)
                        {
                            sum = Convert.ToInt32(TextB9.Text);
                            sum *= 2; TextB9.Text = sum.ToString();
                            TextB12.Text = "";
                        }
                        else
                        {
                            TextB10.Text = TextB12.Text;
                            TextB12.Text = "";
                        }
                    }
                    else
                    {
                        TextB11.Text = TextB12.Text;
                        TextB12.Text = "";
                    }

                }
            }
            //******End of third line*******
            //*****Fourth Line******
            if (TextB14.Text != "")
            {
                if (TextB14.Text == TextB13.Text)
                {
                    sum = Convert.ToInt32(TextB13.Text);
                    sum *= 2; TextB13.Text = sum.ToString();
                    TextB14.Text = "";
                }
                else if (TextB13.Text == "")
                {
                    TextB13.Text = TextB14.Text;
                    TextB14.Text = "";
                }
            }
            //----------------------------------
            if (TextB15.Text != "")
            {
                if (TextB15.Text == TextB14.Text)
                {
                    sum = Convert.ToInt32(TextB14.Text);
                    sum *= 2; TextB14.Text = sum.ToString();
                    TextB15.Text = "";
                }
                else if (TextB14.Text == "")
                {
                    if (TextB15.Text == TextB13.Text)
                    {
                        sum = Convert.ToInt32(TextB13.Text);
                        sum *= 2; TextB13.Text = sum.ToString();
                        TextB15.Text = "";
                    }
                    else if (TextB13.Text == "")
                    {
                        TextB13.Text = TextB15.Text;
                        TextB15.Text = "";
                    }
                    else
                    {
                        TextB14.Text = TextB15.Text;
                        TextB15.Text = "";
                    }

                }
            }
            //--------------------------------------------------
            if (TextB16.Text != "")
            {
                if (TextB16.Text == TextB15.Text)
                {
                    sum = Convert.ToInt32(TextB15.Text);
                    sum *= 2; TextB15.Text = sum.ToString();
                    TextB16.Text = "";
                }
                else if (TextB15.Text == "")
                {
                    if (TextB16.Text == TextB14.Text)
                    {
                        sum = Convert.ToInt32(TextB14.Text);
                        sum *= 2; TextB14.Text = sum.ToString();
                        TextB16.Text = "";
                    }
                    else if (TextB14.Text == "")
                    {
                        if (TextB13.Text == "")
                        {
                            TextB13.Text = TextB16.Text;
                            TextB16.Text = "";
                        }
                        else if (TextB13.Text == TextB16.Text)
                        {
                            sum = Convert.ToInt32(TextB13.Text);
                            sum *= 2; TextB13.Text = sum.ToString();
                            TextB16.Text = "";
                        }
                        else
                        {
                            TextB14.Text = TextB16.Text;
                            TextB16.Text = "";
                        }
                    }
                    else
                    {
                        TextB15.Text = TextB16.Text;
                        TextB16.Text = "";
                    }

                }
            }
            //******End of fourth line*******
        }
        
    }
}