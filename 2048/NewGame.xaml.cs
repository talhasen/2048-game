﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO;

namespace _2048
{
    public partial class Page2 : PhoneApplicationPage
    {
        public Page2()
        {
            InitializeComponent();
        }

       

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            if (TextBoxPlayerName.Text == "" || TextBoxPlayerName.Text == "Please, click here and write a name")
            {
                MessageBox.Show("Player Name area can not be empty before the game stars. Please, write a playername");
            }
            else
            {
                StreamWriter writer = new StreamWriter("./PlayerName.txt");
                writer.WriteLine(TextBoxPlayerName.Text);
                writer.Close();
                NavigationService.Navigate(new Uri("/GamePage.xaml", UriKind.Relative));
            }
        }

        private void TextBoxPlayerName_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TextBoxPlayerName.Text = "";
        }

     

       
    }
}