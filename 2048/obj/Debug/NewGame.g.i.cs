﻿#pragma checksum "C:\Users\talha\documents\visual studio 2013\Projects\2048\2048\NewGame.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "3E93F7FF16025641F166DB8C51272210"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace _2048 {
    
    
    public partial class Page2 : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.StackPanel forImage;
        
        internal System.Windows.Controls.Grid InformationPanel;
        
        internal System.Windows.Controls.TextBlock TextBlockPlayerName;
        
        internal System.Windows.Controls.TextBox TextBoxPlayerName;
        
        internal System.Windows.Controls.Button ButtonStart;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/2048;component/NewGame.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.forImage = ((System.Windows.Controls.StackPanel)(this.FindName("forImage")));
            this.InformationPanel = ((System.Windows.Controls.Grid)(this.FindName("InformationPanel")));
            this.TextBlockPlayerName = ((System.Windows.Controls.TextBlock)(this.FindName("TextBlockPlayerName")));
            this.TextBoxPlayerName = ((System.Windows.Controls.TextBox)(this.FindName("TextBoxPlayerName")));
            this.ButtonStart = ((System.Windows.Controls.Button)(this.FindName("ButtonStart")));
        }
    }
}

